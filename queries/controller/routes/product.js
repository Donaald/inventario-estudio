const router = require('express').Router()

router.get('/', require('../product/getAll'))
router.get('/:id', require('../product/getById'))

module.exports = router;