const GetWithPagination = require('../../services/product/getAll')

const GetAll = async function (req, res) {

    try {

        let { skip, limit } = req.headers

        skipInteger = parseInt(skip)

        if (isNaN(skipInteger)) {
            skipInteger = 0
        }

        limitInteger = parseInt(limit)

        if (isNaN(limitInteger)) {
            limitInteger = 20
        }

        res.send(await GetWithPagination(skipInteger, limitInteger,));

    } catch (err) {

        res.status(500).send(err.message)

    }

}

module.exports = GetAll

