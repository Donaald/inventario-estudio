const GetById = require('../../services/product/getById')
const { NotFoundError } = require('../../services/product/errors/errors')

const getById = async function (req, res) {

    try {

        res.send(await GetById(req.params.id))

    } catch (err) {

        const error = { error: err.message }

        if (err instanceof NotFoundError) {
            res.status(404).send(error)
            return
        }

        res.status(500).send(error)
    }

}

module.exports = getById