const axios = require('../config')

describe('Get by id product', () => {

    beforeAll(async () => {

        await axios.post('/products', {
            name: "product",
            value: 1000
        })

    })

    afterAll(async () => {

        await axios.delete('/products/product')

    })

    test('get product with name is product', async () => {


        const expectedCode = 200
        const { status, data } = await axios.get('/products/product')
        const expectedObject = {
            name: "product",
            value: 1000
        }

        expect(status).toBe(expectedCode)
        expect(data).toStrictEqual(expectedObject)

    })

    test('get not found product', async () => {

        const expectedCode = 404
        const expectedObject = {
            error: 'product doesnt exits',

        }

        try {

            await axios.get('/products/product1')

        } catch (err) {

            const { status, data } = err.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }

    })

})
