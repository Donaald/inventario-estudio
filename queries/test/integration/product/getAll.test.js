const axios = require('../config')

describe('Get all products', () => {

    beforeAll(async () => {

        await axios.post('/products', {
            name: "product1",
            value: 1000
        })

        await axios.post('/products', {
            name: "product2",
            value: 2000
        })

    })

    afterAll(async () => {

        await axios.delete('/products/product1')
        await axios.delete('/products/product2')

    })

    test('get two products in all products', async () => {

        const expectedCode = 200
        const { status, data } = await axios.get('/products')
        const expectedObject = [
            {
                name: "product1",
                value: 1000
            },
            {
                name: "product2",
                value: 2000
            }
        ]

        expect(status).toBe(expectedCode)
        expect(data).toStrictEqual(expectedObject)

    })

})