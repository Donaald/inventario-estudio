const conection = require('../conection')
const MongoError = require('../errors')

const findById = async function (id) {

    try {

        const client = await conection

        return client.db('e-comerse').
            collection('products').
            findOne({ name: id }, { projection: { _id: 0 } })

    } catch (err) {

        throw new MongoError(err.message)

    }

}

module.exports = findById