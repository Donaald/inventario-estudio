const conection = require('../conection')

const findAllBySkipLimit = async function (skip, limit) {
    const client = await conection

    return client.db('e-comerse').
        collection('products').
        find({}, { projection: { _id: 0 } }).
        skip(skip).
        limit(limit).
        toArray()
}

module.exports = findAllBySkipLimit