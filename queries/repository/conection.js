const { MongoClient } = require('mongodb');

const uri = process.env.MONGO_URL

module.exports = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).connect()



