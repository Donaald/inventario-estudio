const findAllBySkipLimit = require('../../repository/product/getAll')

async function GetWithPagination(skip, limit) {
    return findAllBySkipLimit(skip, limit)
}

module.exports = GetWithPagination