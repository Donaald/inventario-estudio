const [
    INCONSISTENCY_RESULT,
    PRODUCT_EXITS,
    PRODUCT_DOESNT_EXITS,

] = [
        'more than one product with the same name',
        'product exits',
        'product doesnt exits',
    ]

module.exports = {
    INCONSISTENCY_RESULT,
    PRODUCT_EXITS,
    PRODUCT_DOESNT_EXITS,
}
