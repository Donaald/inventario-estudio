const findById = require('../../repository/product/findById')
const { NotFoundError } = require('./errors/errors')

async function GetById(id) {

    const product = await findById(id)

    if (product === null) {
        throw new NotFoundError('product doesnt exits')
    }

    return product
}

module.exports = GetById