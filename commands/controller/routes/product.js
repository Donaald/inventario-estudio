const router = require('express').Router()

router.delete('/:id', require('../product/delete'))
router.put('/:id', require('../product/update'))
router.post('/', require('../product/save'))

module.exports = router