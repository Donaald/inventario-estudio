const Delete = require('../../services/product/delete')
const { NotFoundError } = require('../../services/product/errors/errors')

const RestDelete = async function (req, res) {

    try {

        await Delete(req.params.id)
        res.status(204).send()

    } catch (err) {

        const error = { error: err.message }

        if (err instanceof NotFoundError) {
            res.status(404).send(error);
            return
        }

        res.status(500).send(error);
    }

}

module.exports = RestDelete
