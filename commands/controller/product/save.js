const Save = require('../../services/product/save')
const { ProductExits } = require('../../services/product/errors/errors')

const Validator = require('jsonschema').Validator;
let validator = new Validator();
const SaveProductSchema = require("./schemas/saveProduct.json")

const save = async function (req, res) {

    try {

        let product = req.body
        let result = validator.validate(product, SaveProductSchema)

        if (!result.valid) {
            res.status(400).send(
                {
                    error: result.errors.map(error => error.stack.replace("instance.", ""))
                }
            )
            return
        }

        res.send(await Save(product))

    } catch (err) {

        const error = { error: err.message }

        if (err instanceof ProductExits) {
            res.status(404).send(error)
            return
        }

        //logger.error(err)
        res.status(500).send(error)
    }

}

module.exports = save