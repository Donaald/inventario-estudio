const Edit = require('../../services/product/edit')
const Validator = require('jsonschema').Validator;
let validator = new Validator();
const UpdateProductSchema = require("./schemas/updateProduct.json")
const { NotFoundError } = require('../../services/product/errors/errors')

const update = async function (req, res) {

    try {

        let product = req.body
        let { id } = req.params
        product = { ...product, name: id }

        let result = validator.validate(product, UpdateProductSchema)

        if (!result.valid) {
            res.status(400).send(
                {
                    error: result.errors.map(error => error.stack.replace("instance.", ""))
                }
            )
            return
        }

        res.send(await Edit(product))

    } catch (err) {

        const error = { error: err.message }

        if (err instanceof NotFoundError) {
            res.status(404).send(error)
            return
        }

        res.status(500).send(error)
    }

}

module.exports = update

