const customFind = require('../../repository/product/customFind')
const remove = require('../../repository/product/delete')
const send = require('../../publisher/product/publish')
const inconsistencyTopicEventsLogger = require('../../../config/logger/inconsistency-topic-events')
const {
    NotFoundError,
    FailedDelete
} = require('./errors/errors')

const { DELETE } = require('./commands/commands')

const {
    PRODUCT_DOESNT_EXITS,
    FAILED_DELETE,
} = require('./errors/messages')

async function Delete(id) {

    const command = { command: DELETE, data: id }

    try {

        if (await customFind({ name: product.name }) === null) {
            throw new NotFoundError(PRODUCT_DOESNT_EXITS)
        }

        if (!await remove(id)) {
            throw new FailedDelete(FAILED_DELETE)
        }

        send(command)

        return

    } catch (err) {

        if (err instanceof AWSSnsError) {
            inconsistencyTopicEventsLogger.error(command)
        }

        throw new Error(err.message)

    }

}

module.exports = Delete