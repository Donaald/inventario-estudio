const customFind = require('../../repository/product/customFind')
const send = require('../../publisher/product/publish')
const edit = require('../../repository/product/edit')
const inconsistencyTopicEventsLogger = require('../../../config/logger/inconsistency-topic-events')
const { UPDATE } = require('./commands/commands')
const {
    NotFoundError,
    FailedInsert,
    InconsistencyResult
} = require('./errors/errors')
const {
    FAILED_UPDATE,
    INCONSISTENCY_RESULT,
    PRODUCT_DOESNT_EXITS
} = require('./errors/messages')


async function Edit(product) {
    const savedProduct = await customFind({ name: product.name })

    if (savedProduct.length > 1) {
        throw new InconsistencyResult(INCONSISTENCY_RESULT)
    }

    if (savedProduct.length === 0) {
        throw new NotFoundError(PRODUCT_DOESNT_EXITS)
    }

    const command = { command: UPDATE, data: product }

    try {

        if (!await edit(product)) {
            throw new FailedInsert(FAILED_UPDATE)
        }

        send(command)

        return { ...product, _id: undefined }

    } catch (err) {

        if (err instanceof AWSSnsError) {

            inconsistencyTopicEventsLogger.error(command)

        }

    }



}

module.exports = Edit