class NotFoundError extends Error {
    constructor(message) {
        super(message);
        this.name = "NotFoundError";
    }
}

class FailedInsert extends Error {
    constructor(message) {
        super(message);
        this.name = "FailedInsert";
    }
}

class ProductExits extends Error {
    constructor(message) {
        super(message);
        this.name = "ProductExits";
    }
}

class InconsistencyResult extends Error {
    constructor(message) {
        super(message);
        this.name = "InconsistencyResult";
    }
}

class FailedDelete extends Error {
    constructor(message) {
        super(message);
        this.name = "FailedDelete";
    }
}

module.exports = {
    NotFoundError,
    FailedInsert,
    ProductExits,
    InconsistencyResult,
    FailedDelete
}