const [
    FAILED_INSERT,
    INCONSISTENCY_RESULT,
    PRODUCT_EXITS,
    FAILED_DELETE,
    PRODUCT_DOESNT_EXITS,
    FAILED_UPDATE
] = [
        'failed to insert product',
        'more than one product with the same name',
        'product exits',
        'failed to delete product',
        'product doesnt exits',
        'failed to update product'
    ]

module.exports = {
    FAILED_INSERT,
    INCONSISTENCY_RESULT,
    PRODUCT_EXITS,
    FAILED_DELETE,
    PRODUCT_DOESNT_EXITS,
    FAILED_UPDATE
}
