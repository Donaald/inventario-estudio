const save = require('../../repository/product/save')
const customFind = require('../../repository/product/customFind')
const send = require('../../publisher/product/publish')
const AWSSnsError = require('../../publisher/errors')
const logger = require('../../../config/logger/logger')
const inconsistencyTopicEventsLogger = require('../../../config/logger/inconsistency-topic-events')
const { CREATE } = require('./commands/commands')
const {
    FailedInsert,
    ProductExits,
    InconsistencyResult,
} = require('./errors/errors')

const {
    FAILED_INSERT,
    INCONSISTENCY_RESULT,
    PRODUCT_EXITS
} = require('./errors/messages')

async function Save(product) {

    const savedProduct = await customFind({ name: product.name })

    if (savedProduct.length === 1) {
        logger.error(`se intento guardar el  producto ${JSON.stringify(product)} y ya se encontraba en la base de datos`)
        throw new ProductExits(PRODUCT_EXITS)
    }

    if (savedProduct.length > 1) {
        logger.error(`se intento guardar el  producto ${JSON.stringify(product)} y se encontraron datos duplicados en la base de datos`)
        throw new InconsistencyResult(INCONSISTENCY_RESULT)
    }

    const command = { command: CREATE, data: product }

    try {

        isSaved = await save(product)

        if (!isSaved) {
            logger.error(`se generaron errores tecnicos en la base de datos al intentar realizar una insercion`)
            throw new FailedInsert(FAILED_INSERT)
        }

        send(command)

        logger.info(`se guardo con exito el producto: ${JSON.stringify(product)}`)
        return { ...product, _id: undefined }

    } catch (err) {

        if (err instanceof AWSSnsError) {
            inconsistencyTopicEventsLogger.error(command)
        }

        throw new FailedInsert(FAILED_INSERT)

    }

}

module.exports = Save