const [
    CREATE,
    UPDATE,
    DELETE,
] = [
        'create',
        'update',
        'delete',
    ]

module.exports = {
    CREATE,
    UPDATE,
    DELETE
}

