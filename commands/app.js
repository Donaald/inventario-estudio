require('dotenv').config()
const AWS = require('aws-sdk')
const express = require('express')
const app = express();
const product = require('./controller/routes/product')
const port = process.env.PORT

app.use(express.json())
app.use('/products', product)

AWS.config.getCredentials((err) => {
  if (err) {
    console.error(err)
    process.exit(1)
  }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
