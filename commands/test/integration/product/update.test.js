const axios = require('../config')

describe('Save products', () => {

    beforeAll(async () => {

        await axios.post('/products', {
            name: "product",
            value: 1000
        })

    })

    afterAll(async () => {

        await axios.delete('/products/product')

    })

    test('update product with name is product', async () => {


        const expectedCode = 200
        const { status, data } = await axios.put(
            '/products/product',
            {
                value: 2000
            },
        )

        const expectedObject = {
            name: "product",
            value: 2000
        }

        expect(status).toBe(expectedCode)
        expect(data).toStrictEqual(expectedObject)

    })

    test('update not found product', async () => {

        const expectedCode = 404
        const expectedObject = {
            error: 'product doesnt exits'
        }

        try {

            await axios.put('/products/product1', {
                value: 2000
            })

        } catch (err) {

            const { status, data } = err.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }
    })

    test('update product without value', async () => {

        const expectedCode = 400
        const expectedObject = {
            error: ['value is required']
        }

        try {

            await axios.put('/products/product', {})

        } catch (err) {

            const { status, data } = err.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }
    })

})