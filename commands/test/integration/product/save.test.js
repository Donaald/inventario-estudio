const axios = require('../config')

describe('Save products', () => {

    afterAll(async () => {

        await axios.delete('/products/product')

    })

    test('save product with name is product', async () => {


        const expectedCode = 200
        const { status, data } = await axios.post(
            '/products',
            {
                name: "product",
                value: 1000
            },
        )

        const expectedObject = {
            name: "product",
            value: 1000
        }

        expect(status).toBe(expectedCode)
        expect(data).toStrictEqual(expectedObject)

    })

    test('save product with saved product name', async () => {

        const expectedCode = 404
        const expectedObject = {
            error: "product exits"
        }

        try {

            await axios.post(
                '/products',
                {
                    name: "product",
                    value: 1000
                },
            )

        } catch (error) {

            const { status, data } = error.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }

    })

    test('save product without value', async () => {

        const expectedCode = 400
        const expectedObject = {
            error: ['value is required']
        }

        try {

            await axios.post('/products', {
                name: "product"
            })

        } catch (err) {

            const { status, data } = err.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }
    })

    test('save product without name', async () => {

        const expectedCode = 400
        const expectedObject = {
            error: ['name is required']
        }

        try {

            await axios.post('/products', {
                value: 1000
            })

        } catch (err) {

            const { status, data } = err.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }
    })

    test('save product without name and value', async () => {

        const expectedCode = 400
        const expectedObject = {
            error: ['name is required', 'value is required']
        }

        try {

            await axios.post('/products', {

            })

        } catch (err) {

            const { status, data } = err.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }
    })

})