const axios = require('../config')

describe('Delete product', () => {

    beforeAll(async () => {

        await axios.post('/products', {
            name: "product",
            value: 1000
        })

    })

    test('delete product with product name', async () => {

        const expectedCode = 204
        const { status } = await axios.delete('/products/product')

        expect(status).toBe(expectedCode)

    })

    test('delete not found product', async () => {

        const expectedCode = 404
        const expectedObject = {
            error: 'product doesnt exits'
        }

        try {

            await axios.get('/products/product1')

        } catch (err) {

            const { status, data } = err.response

            expect(status).toBe(expectedCode)
            expect(data).toStrictEqual(expectedObject)

        }

    })

})