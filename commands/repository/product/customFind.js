const conection = require('../conection')
const MongoError = require('../errors')

const customFind = async function (query, project) {

    try {
        const client = await conection

        project = { ...project, _id: 0 }

        if (query === undefined) {
            query = {}
        }

        return client.db('e-comerse').
            collection('products').
            find(query, { projection: project }).
            toArray()

    } catch (err) {
        throw new MongoError(err.message)
    }

}

module.exports = customFind