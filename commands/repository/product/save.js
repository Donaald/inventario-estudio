const conection = require('../conection')
const MongoError = require('../errors')
const logger = require('../../../config/logger/logger')

const save = async function (product) {

    try {

        const client = await conection
        const result = await client
            .db('e-comerse')
            .collection('products')
            .insertOne(product)

        return result.acknowledged

    } catch (err) {

        logger.error(err.stack)
        throw new MongoError(err.message)

    }

}

module.exports = save