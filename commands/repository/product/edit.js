const conection = require('../conection')
const MongoError = require('../errors')

const edit = async function (product) {

    try {

        const client = await conection

        return client.db('e-comerse')
            .collection('products')
            .updateOne({ name: product.name }, { $set: product })
            .then(result => result.acknowledged)

    } catch (err) {

        throw new MongoError(err.message)

    }

}

module.exports = edit