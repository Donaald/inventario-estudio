const conection = require('../conection')

const remove = async function (id) {
    const client = await conection

    return client.db('e-comerse').
        collection('products').
        deleteOne({ name: id }).
        then(result => result.acknowledged)
}

module.exports = remove