const AWS = require('aws-sdk')
const AWSSnsError = require('../errors')

const Send = async function (message) {

  return new AWS.SNS({ apiVersion: '2010-03-31' })
    .publish({
      Message: JSON.stringify(message),
      TopicArn: process.env.TOPIC
    })
    .promise()
    .then((response) => {
      console.log(
        'RequestId:', response.ResponseMetadata.RequestId,
        'MessageId:', response.MessageId
      )
    })
    .catch((reason) => { throw new AWSSnsError(reason.message) })

}

module.exports = Send


