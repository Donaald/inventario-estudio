class AWSSnsError extends Error {
    constructor(message) {
        super(message);
        this.name = "AWSSnsError";
    }
}

module.exports = AWSSnsError