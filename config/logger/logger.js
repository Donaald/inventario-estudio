const winston = require('winston')

module.exports = winston.createLogger({
    level: 'info',
    format:
        winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
        ),
    defaultMeta: { service: 'products' },
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'products.log' }),
    ]
})